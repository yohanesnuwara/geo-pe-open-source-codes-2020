# Python Open-Source Codes for Geoscience and Petroleum Engineering Applications 2020

This repository stores all open-source codes for geoscience and petroleum engineering I have created in 2020. 

## It is a big achievement

Since February 2020, I have created handful of Python programs for geoscience and petroleum engineering in my [GitHub](https://github.com/yohanesnuwara/yohanesnuwara). The number of students, academic researchers, and professionals who learned, used, and referred to these codes grew fast. Using these codes, I also gave [courses](https://github.com/yohanesnuwara/python-bootcamp-for-geoengineers) with the Society of Petroleum Engineers (SPE) and in some universities namely Marietta College in Ohio, US, impacting more than 1,000 students and professionals. I also produced several tutorials to use these codes in [LinkedIn](https://www.linkedin.com/in/yohanesnuwara). 

## Organization of this repo

Each folder in this repo resembles different topics.

* `learn-python`: Contains notebooks I use for giving training with SPE, generally aimed for starting to learn Python, including introduction to Python (Numpy, Matplotlib, and Pandas), introductory Python applications for exploration, another one for production, Python applications for formation evaluation, and another one for reservoir engineering analysis.
* `computational-geomechanics`: Contains notebooks of different topics in reservoir geomechanics, such as pore pressure, earth's stress calculation, fracture analysis for rock failure, microseismicity, etc. These notebooks are derived from my assignments in two Reservoir Geomechanics online courses in Stanford EdX by Prof. Mark Zoback.   
* `computational-geophysics`: Contains Python source codes for gravity method (regional-residual Bouguer anomaly separation using Second Vertical Derivative and Fourier Transform) and seismic method (loading 3D seismic data, viewing 2D sections, spectral calculation, and seismic attribute calculation).
* `formation-evaluation`: Contains Python modules for basic viewing of wireline logs, triple combo, scatter plot with Neutron-Density template, formation label attributor.
* `reservoir-engineering`: Contains Python modules and numerical models for variety of basic reservoir engineering analysis. Python modules include material balance plot analysis in dry-gas, gas-condensate, undersaturated, and saturated oil reservoirs (volatile and non-volatile); volumetric mapping; PVT correlations; well-test analysis; and decline curve analysis. Numerical models include aquifer influx model and wellbore transient response. 
* `rock-physics`: Contains Python modules for rock-fluid substitution (Biot-Gassmann and Kuster-Toksoz model), fluid acoustic property (Batzle-Wang), TTI-VTI anisotropic medium stiffness tensor rotation model.

## How to download

Go to [this page](https://bitbucket.org/yohanesnuwara/geo-pe-open-source-codes-2020/downloads/) and Download Repository. You can also clone this repository from your command prompt by `git clone https://yohanesnuwara@bitbucket.org/yohanesnuwara/geo-pe-open-source-codes-2020.git`.

## Notebooks cannot be rendered in Bitbucket

You cannot view the Jupyter notebook files `.ipynb` because Bitbucket doesn't support rendering & viewing. It is best to download or clone this repository, and open these files in your own Jupyter Notebook application, online [Google Colab](https://colab.research.google.com/notebooks/intro.ipynb), or online [nbviewer](https://nbviewer.jupyter.org/). 

## Code Citation

If you use the codes in this repository for any purpose, please cite using the following format. 

> Y. Nuwara, Python Open-Source Codes for Geoscience and Petroleum Engineering Applications, (2020), Bitbucket repository, [https://bitbucket.org/yohanesnuwara/geo-pe-open-source-codes-2020](https://bitbucket.org/yohanesnuwara/geo-pe-open-source-codes-2020)

## Code License

Copyright(C) 2020 Yohanes Nuwara. Licensed international with [Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)