def rotate_stiffness_tensor(S, strike, dip):
  """
  Rotation of 6x6 Stiffness Tensor from assumed "VTI" global coordinate to TTI coordinate

  @author: Yohanes Nuwara
  @email: ign.nuwara97@gmail.com

              Y- (North)
              /
            /
          /
        /
      O -----------> X+ (East)
      |
      |
      |
      |
      v
      Z+ (Depth)

  Bond, W. L. (1943) The Mathematics of the Physical Properties of Crystals. Bell System
  Kostecki, A. (2011) Tilted Transverse Isotropy. Nafta-Gazprom
  """  
  # direction cosines of rotation along Z axis (rotate strike) 
  theta = strike
  a1 = np.cos(np.deg2rad(theta)); a2 = np.sin(np.deg2rad(theta)); a3 = 0
  b1 = -np.sin(np.deg2rad(theta)); b2 = np.cos(np.deg2rad(theta)); b3 = 0
  c1 = 0; c2 = 0; c3 = 1

  Z = np.array([[a1, a2, a3],
                [b1, b2, b3],
                [c1, c2, c3]])

  # direction cosines of rotation along X axis (rotate dip) 
  psi = dip # transformation angle
  d1 = np.cos(np.deg2rad(psi)); d2 = 0; d3 = -np.sin(np.deg2rad(psi))
  e1 = 0; e2 = 1; e3 = 0
  f1 = np.sin(np.deg2rad(psi)); f2 = 0; f3 = np.cos(np.deg2rad(psi))

  X = np.array([[d1, d2, d3],
                [e1, e2, e3],
                [f1, f2, f3]])

  # direction cosine of rotation combination of strike and dip 
  L = np.dot(X, Z)

  # direction cosine elements 
  l1 = L[0][0]; l2 = L[0][1]; l3 = L[0][2]
  m1 = L[1][0]; m2 = L[1][1]; m3 = L[1][2]
  n1 = L[2][0]; n2 = L[2][1]; n3 = L[2][2]

  # rotation tensor 

  Y = np.array([[l1**2,   l2**2,   l3**2,   2*l2*l3,          2*l3*l1,          2*l1*l2],
                [m1**2,   m2**2,   m3**2,   2*m2*m3,          2*m3*m1,          2*m1*m2],
                [n1**2,   n2**2,   n3**2,   2*n2*n3,          2*n3*n1,          2*n1*n2],
                [m1*n1,   m2*n2,   m3*n3,   m2*n3 + m3*n2,    m1*n3 + m3*n1,    m2*n1 + m1*n2],
                [n1*l1,   n2*l2,   n3*l3,   l2*n3 + l3*n2,    l1*n3 + l3*n1,    l1*n2 + l2*n1],
                [l1*m1,   l2*m2,   l3*m3,   l2*m3 + l3*m2,    l1*m3 + l3*m1,    m2*l1 + l2*m1]])

  # transformation of stiffness 
  Y_trans = np.transpose(Y)
  S_dot = np.dot(Y, S)
  S_trans = np.dot(S_dot, Y_trans)  
  
  return S_trans

# Test
# Tensor input
S = np.array([[31.94, 10.67,  8.99,  0.,    0.,    0.  ],
              [10.67, 31.94,  8.99,  0.,    0.,    0.  ],
              [ 8.99,  8.99, 20.48,  0.,    0.,    0.  ],
              [ 0.,    0.,    0.,    6.26,  0.,    0.  ],
              [ 0.,    0.,    0.,    0.,    6.26,  0.  ],
              [ 0.,    0.,    0.,    0.,    0.,   10.63]])

# Dip and strike input
strike = 180 # azimuth, N...E, 138.95
dip = 30 # inclination, 56.87

# Rotate tensor
S_trans = rotate_stiffness_tensor(S, strike, dip)

# compliance tensor of original and rotated
C = np.linalg.inv(S)
C_trans = np.linalg.inv(S_trans)

np.set_printoptions(precision=2, suppress=True)

print('Original stiffness tensor:')
print(S, '\n')

print('Rotated stiffness tensor with strike N {}° E and dip {}°:'.format(strike, dip))
print(S_trans, '\n')

print('Original compliance tensor:')
print(C, '\n')

print('Rotated compliance tensor with strike N {}° E and dip {}°:'.format(strike, dip))
print(C_trans)