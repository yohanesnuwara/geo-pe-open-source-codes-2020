def thomas_stieber(vsh, rhob, rho_clean_sand, rho_shale, 
                   phi_clean_sand, phi_shale, rho_water=1, c=None):
  """
  Thomas-Stieber model for sand-shale system
  """

  # Three points
  ## Point A. Clean sand point
  plt.scatter(0, rho_clean_sand)
  plt.text(0, rho_clean_sand - 0.05, 'Clean Sand')

  ## Point C. Shale point
  plt.scatter(1, rho_shale)

  ## Point B. Sand with pore space completely filled with shale point
  rho_disp = rho_clean_sand + phi_clean_sand * (rho_shale - rho_water)
  vsh_disp = phi_clean_sand
  plt.scatter(vsh_disp, rho_disp)
  plt.text(vsh_disp - 0.08, rho_disp + 0.02, 'Pore-filled w/ shale')

  # Lines
  ## Line AB. Dispersed shale
  plt.plot([0, vsh_disp], [rho_clean_sand, rho_disp], color='purple', 
           label='Sand with Dispersed Shale')

  ## Line BC. Shale with Dispersed Quartz
  plt.plot([vsh_disp, 1], [rho_disp, rho_shale], color='green', 
           label='Shale with Dispersed Quartz')

  ## Line AC. 
  plt.plot([0, 1], [rho_clean_sand, rho_shale], color='blue')

  # V dispersed shale lines
  ## divide line AB into 6 points (0, .2, .4, .6, .8, 1)
  vsh_disp_point = np.linspace(0, vsh_disp, 6)
  rho_disp_point = np.linspace(rho_clean_sand, rho_disp, 6)

  ## Plot multiple lines
  [plt.plot([i0, i1], [j0, j1], color='black') for i0, i1, j0, j1 in \
   zip(vsh_disp_point[1:-1], np.full(4, 1), 
       rho_disp_point[1:-1], np.full(4, rho_shale))]
  
  text = ['$0.2 \phi$', '$0.4 \phi$', '$0.6 \phi$', '$0.8 \phi$']
  [plt.text(i, j, k) for i, j, k in zip(vsh_disp_point[1:-1]-0.07, 
                                        rho_disp_point[1:-1], text)]

  # NTG lines
  ## divide line BC into 6 points (0, .2, .4, .6, .8, 1)
  vsh_ntg_point1 = np.linspace(vsh_disp, 1, 6)
  rho_ntg_point1 = np.linspace(rho_disp, rho_shale, 6) 

  ## divide line AC into 6 points (0, .2, .4, .6, .8, 1)
  vsh_ntg_point2 = np.linspace(0, 1, 6)
  rho_ntg_point2 = np.linspace(rho_clean_sand, rho_shale, 6)  

  ## Plot multiple lines
  [plt.plot([i0, i1], [j0, j1], color='black') for i0, i1, j0, j1 in \
   zip(vsh_ntg_point1[1:-1], vsh_ntg_point2[1:-1], 
       rho_ntg_point1[1:-1], rho_ntg_point2[1:-1])] 

  text = ['$0.2 N/G$', '$0.4 N/G$', '$0.6 N/G$', '$0.8 N/G$']
  [plt.text(i, j, k) for i, j, k in zip(vsh_ntg_point2[1:-1]-0.04, 
                                        rho_ntg_point2[1:-1]-0.04, text)] 
  
  # Data points
  plt.scatter(vsh, rhob, c=c)
  plt.colorbar()

  plt.xlim(0, 1); plt.ylim(1.95, 2.95)
  plt.xlabel('Shale Volume [v/v]'); plt.ylabel('Bulk Density [g/cc]')

  plt.title('Thomas-Stieber Model for Sand-Shale System', pad=15)
  plt.legend(prop={'size': 10})
  plt.grid()
  plt.show()

## Test
# z1 = np.linspace(1500, 1700, 100)
# x1 = np.random.normal(0.5, 0.1, 100)
# y1 = np.linspace(2.3, 2.4, 100)

# z2 = np.linspace(1701, 1900, 100)
# x2 = np.random.normal(0.2, 0.05, 100)
# y2 = np.linspace(2.4, 2.5, 100)

# z3 = np.linspace(1901, 2100, 100)
# x3 = np.random.normal(0.4, 0.05, 100)
# y3 = np.linspace(2.6, 2.72, 100)

# vsh = np.concatenate((x1, x2, x3))
# rhob = np.concatenate((y1, y2, y3))
# depth = np.concatenate((z1, z2, z3))

## Produce Thomas-Stieber plot
#thomas_stieber(vsh, rhob, 2.2, 2.5, 0.35, 0.15, c=depth)