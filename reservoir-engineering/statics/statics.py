def rhooilgrad(so, sg, Rs, Bo):
  # standard conditions

  temp_sc = 60 # in fahrenheit
  pressure_sc = 14.73 # 1 atm = 14.73 psia
  z_sc = 1 # gas z factor at standard condition
  
  Rs_converted = 940 * (1/5.6148) # convert to scf oil/scf gas

  # oil FVF at surface/standard condition using Standing correlation (Equation 2.37 See Unit 2)

  F = Rs * ((sg / so)**0.5) + (1.25 * temp_sc) # Rs must be in scf/STB
  Bo = 0.972 + (0.000147*(F**1.1756))

  # oil density at surface/standard condition

  rhowater = 62.366 # 1 g/cm3 = 62.366 lbm/ft3
  rhooil_sc = so * rhowater

  # gas density at surface/standard condition (Eq 2.23, real-gas law)

  R = 10.732 # gas constant in (ft3*psi)/(lb-mol*R)
  rhogas_sc = (28.97 * sg * pressure_sc) / (z_sc * R * (temp_sc + 459)) # temp converted to Rankine

  # finally, oil density at reservoir condition

  rhooil = (rhooil_sc + (rhogas_sc * Rs_converted)) / Bo

  # oil density gradient

  rhooil_grad = rhooil / 144 # 144 is factor conversion from density lbm/ft3 to psi/ft
  rhooil_grad_converted = rhooil_grad * (6.89476 / 0.3048) # convert from psi/ft to kPa/m
  return(rhooil_grad, rhooil_grad_converted)

def extrapolatepressure_gas(sg, pressure, temp, delta):
  import numpy as np
  R = 10.732

  rhogas = (28.97 * sg * pressure) / (z * R * (temp + 459)) # temp convert to Rankine

  # gas density gradient

  rhogas_grad = rhogas / 144

  # extrapolate using Eq 3.1

  pressure_extrapolated_below = pressure + rhogas_grad * delta
  pressure_extrapolated_above = pressure - rhogas_grad * delta
  
  # or extrapolate using Eq 3.7
  
  pressure_extrapolated_below2 = pressure*(np.exp((0.01877 * sg * delta) / (z * (temp + 459)))) # temp in Rankine
  pressure_extrapolated_above2 = pressure*(np.exp(-(0.01877 * sg * delta) / (z * (temp + 459)))) # temp in Rankine
  
  return(pressure_extrapolated_below, pressure_extrapolated_above, pressure_extrapolated_below2, pressure_extrapolated_above2)